import Post from "../Reducer/ReduxProduct";
import { combineReducers } from "redux";

const CombinedReducers = combineReducers({
    post: Post
})

export default CombinedReducers;