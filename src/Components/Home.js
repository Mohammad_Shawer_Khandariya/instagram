import React from "react";
import axios from "axios";
import Post from "../Action/Action";
import { connect } from "react-redux";

const mapState = (props) => {
    return {
        post: props.post,
    }
}

const mapDispatch = (dispatch) => {
    return {
        postData: () => dispatch(Post()),
    }
}

class Home extends React.Component {
 
    async fetchPost() {
        const postRes = await axios.get('https://jsonplaceholder.typicode.com/posts')
        const post = postRes.data;
        // console.log(postData);
        await this.props.postData(post)
    }

    componentDidMount() {
        this.fetchPost()
    }

    render() {
        const posts = this.props.post;
        console.log(posts);

        return (
            <div>Hello
                {
                    posts.slice(0, 10).map((post) => {
                        return (
                            <div className="" key={post.id}>
                                {JSON.stringify(post)}
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default connect(mapState, mapDispatch)(Home);