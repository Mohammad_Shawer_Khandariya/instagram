import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom/cjs/react-router-dom.min';
import Navbar from './Components/Navbar';
import Home from './Components/Home';
// import './App.css';

export default class App extends React.Component {

  render() {
    return (
      <div className="App ">
        {/* <header className="App-header">
          <h1 className='text-alert' >React Bootstrap</h1>
        </header> */}
        <Router>
          <Navbar />
          <Switch>
            <div className='bg-light container'>
              <Route exact path="/" ><Home /></Route>
              <Route exact path="/direct" >Messenger</Route>
              <Route exact path="/create" >Upload</Route>
              <Route exact path="/explore" >Explore</Route>
            </div>
          </Switch>
        </Router>
      </div>
    )
  }
}