const data = require("../Components/Post")

const Post = () => {
    return {
        type: "Post",
        payload: data,
    }
}

export default Post;