import CombinedReducers from "../CombinedReducer/CombinedReducer";
import { createStore } from "redux";

const Store = createStore(CombinedReducers);

export default Store;